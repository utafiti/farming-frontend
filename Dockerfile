FROM node:12-alpine

WORKDIR /usr/src/app

COPY package.json ./
RUN apk --no-cache add git

RUN yarn 

COPY / ./

RUN yarn global add serve

EXPOSE 5000

RUN chmod +x run.sh

ENTRYPOINT ["/usr/src/app/run.sh"]