import { values } from 'lodash'

export const UNISWAP_V2_WETH_FOX_POOL_ADDRESS = '0x60321af5e515a5B12ecc310aad5Ae2801Acdc029'
export const UNISWAP_V2_USDC_ETH_POOL_ADDRESS = '0x16b9a82891338f9ba80e2d6970fdda79d1eb0dae'
export const UNISWAP_V2_ROUTER = '0x10ED43C718714eb63d5aA57B78B54704E256024E'
export const FOX_TOKEN_CONTRACT_ADDRESS = '0x362206fa7fd6ae63a82afb573e69e89965a3e413'
export const WETH_TOKEN_CONTRACT_ADDRESS = '0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c'

export const FOX_ETH_TEST_FARMING_ADDRESS = ''
export const FOX_ETH_FARMING_ADDRESS = '0xae5Ac3701331Bb21E87Df3Ea3e4a9d19919526BC'
export const ETH_BASE = 0o67405553164731000000
export const MAX_ALLOWANCE = '0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF'

export const COUNT_DOWN_TIME = 1626447600 * 1000 // July 16 2021 9am MST

export const ICHI_ONEFOX_API = 'https://api.ichi.org/v1/farms/1015'

export const FEATURE_FLAGS = {
  airdrop: false,
  ethFoxStakingV1: true,
  ethFoxStakingV2: true
}

type TokenProps = {
  symbol: string
  icon: string
}

export type PoolProps = {
  name: string
  owner: string
  network: string
  token0: TokenProps
  token1: TokenProps
  contractAddress: string
  balance: number
  rewards?: TokenProps[]
}

const poolsContractData = {
  '0x60321af5e515a5B12ecc310aad5Ae2801Acdc029': {
    name: 'BNB-JAMHURI',
    owner: 'cake-LPs',
    network: 'BSC',
    balance: 10,
    token0: {
      symbol: 'BNB',
      icon: 'https://reiggaso.sirv.com/bnb.jpeg'
    },
    token1: {
      symbol: 'JAMUHURI',
      icon: 'https://reiggaso.sirv.com/Images/wavu%20logo_icon.svg'
    },
    contractAddress: '0x60321af5e515a5B12ecc310aad5Ae2801Acdc029',
    rewards: [
      {
        symbol: 'BNB',
        icon: 'https://reiggaso.sirv.com/bnb.jpeg'
      },
      {
        symbol: 'JAMUHURI',
        icon: 'https://reiggaso.sirv.com/Images/wavu%20logo_icon.svg'
      }
    ]
  }
  // '0x16b9a82891338f9ba80e2d6970fdda79d1eb0dae': {
  //   name: 'USDC-JAMHURI',
  //   owner: 'cake-LPs',
  //   network: 'BSC',
  //   balance: 10,
  //   token0: {
  //     symbol: 'USDC',
  //     icon: 'https://assets.coincap.io/assets/icons/256/eth.png'
  //   },
  //   token1: {
  //     symbol: 'JAMUHURI',
  //     icon: 'https://reiggaso.sirv.com/Images/wavu%20logo_icon.svg'
  //   },
  //   contractAddress: '0x16b9a82891338f9ba80e2d6970fdda79d1eb0dae',
  //   rewards: [
  //     {
  //       symbol: 'USDC',
  //       icon: 'https://assets.coincap.io/assets/icons/256/eth.png'
  //     },
  //     {
  //       symbol: 'JAMUHURI',
  //       icon: 'https://reiggaso.sirv.com/Images/wavu%20logo_icon.svg'
  //     }
  //   ]
  // }
}

export const poolContracts = values(poolsContractData) as unknown as PoolProps[]

export type StakingContractProps = {
  name: string
  owner: string
  contractAddress: string
  pool: PoolProps
  network: string
  periodFinish: number
  balance: number
  rewards: TokenProps[]
  enabled?: boolean
}

export const stakingContracts = [
  // {
  //   name: 'USDC-BNB',
  //   owner: 'ShapeShift',
  //   contractAddress: '0xdd80e21669a664bce83e3ad9a0d74f8dad5d9e72',
  //   pool: poolsContractData['0x16b9a82891338f9ba80e2d6970fdda79d1eb0dae'],
  //   periodFinish: 1634223324,
  //   balance: 2000,
  //   network: 'ethereum',
  //   rewards: [
  //     {
  //       symbol: 'FOX',
  //       icon: 'https://reiggaso.sirv.com/Images/wavu%20logo_icon.svg'
  //     }
  //   ],
  //   enabled: FEATURE_FLAGS.ethFoxStakingV1
  // },
  {
    name: 'BNB-JAMUHURI V2',
    owner: 'Utafiti team',
    contractAddress: '0xae5Ac3701331Bb21E87Df3Ea3e4a9d19919526BC',
    // test contract
    // contractAddress: '0x6327fa640ecf1ab1967eb12c7b3494fc269a20b9',
    // expired contract
    // contractAddress: '0x7479831e095481cE46d378Ec6C5291e59BF25A76',
    pool: poolsContractData['0x60321af5e515a5B12ecc310aad5Ae2801Acdc029'],
    periodFinish: 1650625597,
    network: 'BSC',
    balance: 0,
    rewards: [
      {
        symbol: 'SAVANNA',
        icon: 'https://reiggaso.sirv.com/Images/wavu%20logo_icon.svg'
      }
    ],
    enabled: FEATURE_FLAGS.ethFoxStakingV2
  }
] as StakingContractProps[]
