import { Route, Switch } from 'react-router'
import { Text, Link } from '@chakra-ui/react'
import { Hero } from 'Molecules/Hero'
import { AirdropFoxIcon } from 'Atoms/Icons/AirdropFoxIcon'
import { Layout } from 'Molecules/Layout/Layout'
import { LayoutContent } from 'Atoms/LayoutContent'
import { LayoutCard } from 'Atoms/LayoutCard'
import { Opportunities } from './Opportunities'
import { LiquidityRoutes } from './Liquidity/LiquidityRoutes'
import { APRFluctuates } from './APRFluctuates'
import { useMemo } from 'react'
import { supportedContractsPath } from 'utils/helpers'
import { poolContracts } from 'lib/constants'
import { NotFound } from 'pages/NotFound/NotFound'
import { FarmCTA } from 'Atoms/FarmCTA'

export const Farm = () => {
  const supportedContracts = useMemo(() => {
    return supportedContractsPath(poolContracts)
  }, [])

  return (
    <Layout>
      <Hero>
        <AirdropFoxIcon color='white' size='82px' mb={4} />
        <Text
          color='white'
          mb='2'
          textAlign='center'
          fontWeight='light'
          fontSize={{ base: '4xl', md: '5xl' }}
          as='h1'
        >
          JAMHURI has <b>Super</b> Powers
        </Text>
        <Text color='gray.400' textAlign='center' fontWeight='heading' as='p'>
          HODL your JAMHURI Tokens for amazing benefits
        </Text>
        {/* <FarmCTA /> */}
        {/* find ct attoms */}
      </Hero>
      <LayoutContent>
        <LayoutCard>
          <Switch>
            <Route exact path='/farming' component={Opportunities} />
            <Route
              path={`/farming/liquidity/:liquidityContractAddress${supportedContracts}`}
              component={LiquidityRoutes}
            />
            <Route component={NotFound} />
          </Switch>
        </LayoutCard>
        <Route exact path='/farming'>
          <Link color='primary' mb={16} href='https://utafiti.co' target='_blank'>
            How does this work?
          </Link>
        </Route>
        <Route
          path='/farming/liquidity/:liquidityContractAddress/staking/:stakingContractAddress'
          component={APRFluctuates}
        />
      </LayoutContent>
    </Layout>
  )
}
